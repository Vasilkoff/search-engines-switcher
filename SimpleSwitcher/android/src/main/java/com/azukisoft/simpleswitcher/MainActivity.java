package com.azukisoft.simpleswitcher;

import android.app.Activity;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.provider.Settings.Secure;


public class MainActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private Menu optionsMenu;
    private WebView webView = null;
    AdView adView;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);


        if (checkInternetConnection()) {
            webView.loadUrl("http://google.com");
        }



        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        adView = new AdView(this, AdSize.BANNER, "a153232d239323b");
        FrameLayout layout = (FrameLayout)findViewById(R.id.container);
        layout.addView(adView);
        AdRequest request = new AdRequest();
        String devID = Secure.getString(this.getContentResolver(),
                Secure.ANDROID_ID);

      // request.addTestDevice(devID); // My T-Mobile G1 test phone
       // adView.loadAd(request);
        (new Thread() {
            public void run() {
                Looper.prepare();
                adView.loadAd(new AdRequest());
            }
        }).start();
    }

    public boolean checkInternetConnection() {

        if (!isNetworkAvailable(this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setPositiveButton(R.string.ok,new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which) {

                }
            })
                    .setTitle(R.string.error_sending_title)
                    .setMessage(R.string.error_sending_message)
                    .show();
            return false;
        } else {

            webView = (WebView) findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView wView, String url) {
                    return false;
                }


                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                /*    if (optionsMenu != null) {
                        final MenuItem refreshItem = optionsMenu
                                .findItem(R.id.action_waiting);
                        refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
                    }*/
                }
                @Override
                public void onPageFinished(WebView view, String url) {
                 /*   if (optionsMenu != null) {
                        // do your stuff here
                        final MenuItem refreshItem = optionsMenu
                                .findItem(R.id.action_waiting);
                        refreshItem.setActionView(null);
                    }*/
                }
            });
            return true;
        }
    }

    /**
     * check if device is connected
     * @param context
     * @return true if any connection exists
     */
    public boolean isNetworkAvailable(Context context) {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                if (webView!=null) {
                    webView.loadUrl("http://google.com");
                }
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                if (webView!=null) {
                    webView.loadUrl("http://yahoo.com");
                }
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                if (webView!=null) {
                    webView.loadUrl("http://bing.com");
                }
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            this.optionsMenu = menu;
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (item.getItemId() == R.id.action_reload) {
            if (webView!=null) {
                webView.reload();
            } else {
                if (checkInternetConnection()) {
                    webView.reload();
                }
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }


    }

}
